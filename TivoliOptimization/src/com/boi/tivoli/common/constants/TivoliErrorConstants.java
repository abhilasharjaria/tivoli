/*
 * TivoliErrorConstants.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.constants;

/**
 * This class will be having all the constants used for error handling in Tivoli
 * application.
 */
public class TivoliErrorConstants {

	public static final String TIVOLI_MSG_ERROR_ROOT_LOGGER = "root_logger_error";
	public static final String TIVOLI_MSG_ERROR_CODE = "errorCode";
	public static final String TIVOLI_MSG_ALERT_NULL = "tivoli_alert_null";
	public static final String TIVOLI_MSG_IO_EXCEPTION = "tivoli_io_exception";
	public static final String TIVOLI_MSG_INTRUPTED_EXCEPTION = "tivoli_interuppeted_exception";
	public static final String TIVOLI_MSG_EXCEPTION = "tivoli_exception";
	public static final String TIVOLI_MSG_STACKTRACE = "stacktrace";
	public static final String TIVOLI_MSG_WRITE_EXCP = "writeexception";
	public static final String TIVOLI_MSG_VO_NULL = "valueobject_null";
	public static final String TIVOLI_MSG_NO_EMAILL = "no_email_message";
	public static final String TIVOLI_MSG_NO_WRITER = "writer_not_available";
	public static final String TIVOLI_MSG_NO_DATABASE_DATA = "no_data_in_database";
	public static final String TIVOLI_MSG_DATABASE_STORE = "store_database_error";
	public static final String TIVOLI_MSG_DATABASE_RETRIVE = "retrive_database_error";
	public static final String TIVOLI_MSG_DATABASE_CONNECTION = "database_con_error";
	public static final String TIVOLI_MSG_DATABASE_CONN_CLOSE = "database_con_close";
	public static final String TIVOLI_MSG_DATABASE_SQL_EXCP = "database_sql_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTBASEDAO = "tivolialertbaseDAO_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTADAPTER = "tivolialertadapter_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTBUSSINESSBEAN = "tivolialertbussinessbean_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTDAOIMPL = "tivolialertdaoimpl_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTCONTROLLER = "tivolialertcontroller_excp";
	public static final String TIVOLI_MSG_TIVOLIMONITORDIRECTORY = "tivolialertmonitordirectory_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTWRITELOG = "tivolialertwritelog_excp";
	public static final String TIVOLI_MSG_TIVOLIEXCEPTIONHANDLER = "tivoliexceptionhandler_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTEXCEPTION = "tivolialertexception_excp";
	public static final String TIVOLI_MSG_TIVOLIALERT_EXCEPTION = "value not found";
	public static final String TIVOLI_MSG_TIVOLIALERTISMWRITER = "tivolialertismwriter_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTPROPERTIES = "tivolialertproperties_excp";
	public static final String TIVOLI_MSG_TIVOLIALERT_PROPERTIES = "property instamce cannot be loaded";
	public static final String TIVOLI_MSG_TIVOLIALERTEMAILSENDERUTILITY = "tivolialertemailsenderutility_excp";
	public static final String TIVOLI_MSG_MOVEFILE = "moving file failed";
	public static final String TIVOLI_MONITOR_DIR_UNAVAILABLE = "Monitor directory is not available";
	public static final String TIVOLI_TEMP_DIR_UNAVAILABLE = "Temp directory is not available";
	public static final String TIVOLI_MSG_TIVOLIALERTUTIL = "tivolialertutil_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTEXCELWRITER = "tivolialertexcelwriter_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER = "tivolialertstoragebackupcontroller_excp";
	public static final String TIVOLI_MSG_TIVOLIALERTSTORAGEFILERENAME = "AlertStorage file rename failed";
	public static final String TIVOLI_MSG_TIVOLIALERTSTORAGEFILEBACKUPEXISTS = "AlertStorage file backup already exists";
	public static final String TIVOLI_MSG_TIVOLIALERTSTORAGEFILECREATE = "New AlertStorage file creation failed";
	public static final String TIVOLI_MSG_TIVOLIALERTSTORAGEFILECOPY = "AlertStorage file backup failed";
}
