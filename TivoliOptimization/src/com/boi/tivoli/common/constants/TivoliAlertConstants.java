/*
 * TivoliAlertConstants.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */

package com.boi.tivoli.common.constants;

import org.apache.log4j.Logger;

/**
 * This class will be having all constants which are used in Tivoli application.
 */
public final class TivoliAlertConstants {

	// To field for Email
	public static final String MAIL_TO_RECIPIENT_ADDRESS = "to";
	// CC field for Email
	public static final String MAIL_CC_RECIPIENT_ADDRESS = "cc";
	// From Field for Email
	public static final String MAIL_FROM_ADDRESS = "from";
	// Email Template
	public static final String TIVOLI_EMAIL_TEMPLATE = "emailtemplate";
	// Mail Server localhost
	public static final String localhost = "localhost";
	// Mail Server
	public static final String mailserver = "mailserver";
	// Database Driver
	public static final String driver = "driver";
	// Database
	public static final String database = "database";
	// Database user
	public static final String dbuser = "dbuser";
	// Database password
	public static final String dbpassword = "dbpassword";
	// ISM Log text file
	public static final String ismlog = "ismlog";
	// Iteration period time
	public static final String lIterationPeriod = "lIterationPeriod";
	// Maximum count field
	public static final String lMaxIterationCount = "lMaxIterationCount";
	// Watch directory path
	public static final String monitor = "monitor";
	// Configuration properiy file path
	public static final String TivoliAlertProperties = "config/tivolialertconfig.properties";
	// Subject for Email
	public static final String TIVOLI_EMAIL_SUBJECT = "subject";
	// Root Logger
	public static final String TIVOLI_ROOT_LOGGER = "";
	// Number of row count in database
	public static boolean CHECK_SITUATION_ROW_COUNT = false;
	// Write to error log object
	public static Logger errorlog = null;
	// Write to application log object
	public static Logger applicationlog = null;
	// move file to success folder
	public static final String successloc = "successloc";
	// move file to fail folder
	public static final String failloc = "failloc";
	// log4j.properties path
	public static final String logj = "logj";
	// errorlog constant variable
	public static final String error_log = "errorlog";
	// polling Interval constant variable
	public static final String folderMonitorInterval = "folderMonitorInterval";
	// alertThreshold is alertcount more than this value then don't send mail
	public static final String alertThreshold = "alertThreshold";
	//timeDifference is time within which alert count should be checked. ex. 5 means alerts count in last  five mins. 
	public static final String timeDifference = "timeDifference";

	public static final String alertStorageExcel = "alertStorageExcel";
	
	public static final String alertStorageExcelBackupFolder = "alertStorageExcelBackupFolder";
	
	public static final String alertStorageExcelBackupFileName = "alertStorageExcelBackupFileName";
	
	//alertconfigfile is file where all alert's config is stored
	public static final String alertconfigfile = "alertconfigfile";
	// To field for Email
	public static final String EMAIL_TO = "_email_to";
	// CC field for Email
	public static final String EMAIL_CC = "_email_cc";
	
	public static String TIVOLI_ALERT = "TIVOLI_ALERT";
	// tivoli java utitlity running status check log  file
	public static final String tivoliUtilityLog = "tivoliUtilityLog";
	
	public static final String tivoliUtilityLogTime = "tivoliUtilityLogTime";
	
	public static final String PROP_NULLLOGSYSTEM_CLASS = "org.apache.velocity.runtime.log.NullLogSystem";
	public static final String EMAIL_TEMPLATE_PATH = "emailtemplatepath";
}