/*
 * TivoliAlertLogWriter.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.boi.tivoli.common.constants.TivoliAlertConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;

/**
 * This class will be responsible for writing log into log file to check if Tivoli monitoring java utility is running successfully or not. 
 * It is scheduled time based activity (ex. after every 15 mins. log will be written). If java utility stops due to any exception/error , this task will be terminated.
 */
public class TivoliAlertLogWriter extends TimerTask {
	private boolean islogwrite = false;
	private String tivoliLogStr = null;
	private File file = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertLogWriter.class);
	public static Boolean cIsDebug = false;
	private static final String newLine = System.getProperty("line.separator");
	
	public TivoliAlertLogWriter() {
		cIsDebug = cLogger.isDebugEnabled();
	}
	
	/**
	 * This overridden method will call another method to write log in file.
	 */
	public void run() {
		if (cIsDebug){
			cLogger.debug("Writing log in tivoliUtilityLog file at time : " + new Date());
		}
		writeToUtilityLogFile();
	}

	/**
	 * This  method will  write log in file and also checks if file size is greater than 5mb .Is yeas then will create new empty log file
	 */
	public boolean writeToUtilityLogFile() {
		try {
			//2018-05-10 16:51:46 - INFO -  Tivoli Optimization utility is running successfully.
			StringBuilder sbLogMessage = new StringBuilder();
			sbLogMessage = sbLogMessage.append(new Date()).append("- INFO -  Tivoli Optimization utility is running successfully.").append(newLine);
			tivoliLogStr = sbLogMessage.toString();
			TivoliAlertProperties prop = TivoliAlertProperties.getInstance();
			file = new File(prop.getProperty(TivoliAlertConstants.tivoliUtilityLog));

			if (!file.exists()) {
				file.createNewFile();
				checkfileSizeAndWrite(file);
			} else {
				checkfileSizeAndWrite(file);
			}
			islogwrite = true;
			
		} catch (IOException e) {
			cLogger.error(e);
		} catch (TivoliAlertException e) {
			cLogger.error(e);
		}
		if (cIsDebug){
			cLogger.debug("TivoliAlertLogWriter writeToUtilityLogFile() method End ::: ");
		}
		return islogwrite;
	}

	public void checkfileSizeAndWrite(File file) {
		// 5 MB = 5242880 bytes
		double bytes = file.length();
		double kilobytes = (bytes / 1024);
		double megabytes = (kilobytes / 1024);
		PrintWriter out = null;
		try {
			if (megabytes < 5 ) {
				
				out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			    out.println(tivoliLogStr);
			    
			} else {
				file.createNewFile();
				out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			    out.println(tivoliLogStr);
			    out.flush();
			    out.close();
			    
			}
			if (cIsDebug){
				cLogger.debug("Tivoli Utility log file is updated successfully....");
			}
		} catch (IOException e) {
			cLogger.error(e);
		}finally {
	        if (out != null) {
	        	out.flush();
	        	out.close();
	        }
		}
	}
}
