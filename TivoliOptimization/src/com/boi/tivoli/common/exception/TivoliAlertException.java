/*
 * TivoliAlertException.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.exception;

import java.util.HashMap;

import com.boi.tivoli.common.constants.TivoliErrorConstants;

/**
 * This class will be handling exception occurred, an instance of this class is
 * created by passing an error code and stack trace as parameters to its
 * constructor. This instance is used by TivoliAlertWriteLog class for logging
 * the error information to the error log file.
 */
public class TivoliAlertException extends Exception {

	private static final long serialVersionUID = 1L;
	private HashMap<String, String> cErrorInfo = null;

	/**
	 * Only constructor to Initialize the TivoliAlertException.
	 */

	public TivoliAlertException(String pErrorCode, String pStackTrace)
			throws TivoliAlertException {
		try {
			cErrorInfo = new HashMap<String, String>();
			cErrorInfo.put(TivoliErrorConstants.TIVOLI_MSG_ERROR_CODE,
					pErrorCode);
			cErrorInfo.put(TivoliErrorConstants.TIVOLI_MSG_STACKTRACE,
					pStackTrace);
		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEXCEPTION,
					e.toString());
		}
	}

	/**
	 * Get the error detail from map corresponding to key passed in argument.
	 * 
	 */
	public String getErrorInfo(String pErrorKey) throws TivoliAlertException {

		try {
			if (cErrorInfo.containsKey(pErrorKey)) {
				return cErrorInfo.get(pErrorKey);
			} else {
				throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEXCEPTION,
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERT_EXCEPTION);
			}
		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEXCEPTION,
					e.toString());
		}
	}
}