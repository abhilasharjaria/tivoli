package com.boi.tivoli.business;

/*
 * TivoliAlertBussinessBean.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;

import com.boi.tivoli.common.constants.TivoliAlertConstants;
import com.boi.tivoli.common.constants.TivoliErrorConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;
import com.boi.tivoli.common.util.TivoliAlertExcelUtil;
import com.boi.tivoli.common.util.TivoliAlertExcelWriter;
import com.boi.tivoli.common.util.TivoliAlertProperties;
import com.boi.tivoli.common.util.TivoliEmailSenderUtility;
import com.boi.tivoli.vo.TivoliAlertInformationVO;


public class TivoliAlertBussinessBean {

	private TivoliAlertExcelUtil tivoliAlertExcelUtil  = null;
	private boolean emailprinted = false;
	private boolean hievent = false;
	private boolean validateResult = false;
	private String to = null;
	private String cc = null;
	private String from = null;
	private String subject = null;
	private String actionreq = null;
	private StringWriter swriter = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertBussinessBean.class);
	public Boolean cIsDebug = false;

	public TivoliAlertBussinessBean() {
		tivoliAlertExcelUtil = TivoliAlertExcelUtil.getInstance();
		cIsDebug = cLogger.isDebugEnabled();
	}

	public TivoliAlertInformationVO handleAlertIdentifyEvent(
			String pTivoliAlert, TivoliAlertInformationVO taInformationVO) throws TivoliAlertException {

		// Replace with Excel sheet reader , fetch the value from excel sheet and set in VO...
		try {
			taInformationVO = tivoliAlertExcelUtil.searchAlert(pTivoliAlert);
		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSSINESSBEAN,
					e.toString());
		}
		return taInformationVO;

	}

	public boolean handleAlertValidationEvent(
			TivoliAlertInformationVO taInformationVO) throws TivoliAlertException {
		try {
			validateResult = tivoliAlertExcelUtil.checkAlertValidation(
					taInformationVO);
			if (cIsDebug) {
				cLogger.debug("Alert's validateResult in TivoliAlertBussinessBean.handleAlertValidationEvent():::" +validateResult);
			}

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return validateResult;

	}
	/**
	 * This method handles the activity to Build Email template for Tivoli
	 * alert.
	 */
	public StringWriter handleTivoliEmailTemplateEvent(
			TivoliAlertInformationVO taInformationVO)
					throws TivoliAlertException {

		try {
			if (cIsDebug) {
				cLogger.debug("Alert's email template build in TivoliAlertBussinessBean.handleTivoliEmailTemplateEvent() start::");
			}

			TivoliAlertProperties prop = TivoliAlertProperties.getInstance();
			
			Properties p = new Properties();   
	        p.setProperty("resource.loader","file");   
	        p.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");   
	        p.setProperty("file.resource.loader.path", prop.getProperty(TivoliAlertConstants.EMAIL_TEMPLATE_PATH));              
	        p.setProperty("file.resource.loader.cache", "false");   
	        p.setProperty("file.resource.loader.modificationCheckInterval", "0");   

	        Velocity.init(p);      
	        Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, TivoliAlertConstants.PROP_NULLLOGSYSTEM_CLASS);
	        Template t = Velocity.getTemplate(prop.getProperty(TivoliAlertConstants.TIVOLI_EMAIL_TEMPLATE));
	        
	        VelocityContext context = new VelocityContext();

			context.put("alertstring", taInformationVO.getAlertMessage());
			context.put("situation_id", taInformationVO.getSITUATION_ID_PK());
			context.put("configuration_item",
					taInformationVO.getCONFIGURATION_ITEM());
			context.put("it_service", taInformationVO.getIT_SERVICE_IMPACTED());
			context.put("category", taInformationVO.getCATEGORY());
			context.put("bussiness_service",
					taInformationVO.getBUSINESS_IMPACTED());
			context.put("error_catalog_link",
					taInformationVO.getERROR_CATALOG_LINK());
			context.put("onCallNumber", taInformationVO.getonCallNumber());
			context.put("severity", taInformationVO.getSEVERITY());
			context.put("ismresolvergroup",
					taInformationVO.getRESOLVER_GROUP());
			context.put("configitem",
					taInformationVO.getCONFIGURATION_ITEM());
			context.put("old_inc_ref",
					taInformationVO.getOLD_INC_REFERENCE());

			if (taInformationVO.getSEVERITY().equalsIgnoreCase("ignore")) {
				actionreq = "Action Not Required ";
			} else {
				actionreq = "Action Required ";
			}
			swriter = new StringWriter();
			t.merge(context, swriter);
			if (cIsDebug) {
				cLogger.debug("Alert's email template built successfully in TivoliAlertBussinessBean.handleTivoliEmailTemplateEvent() End ::");
			}

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSSINESSBEAN,
					e.toString());
		}
		if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessBean handleInsertEvent() method End ::: ");
		}
		return swriter;
	}

	/**
	 * This method handles the activity to process Email template for Tivoli
	 * alert.
	 */
	public boolean handleEmailSenderEvent(StringWriter pEmailTemplate , TivoliAlertInformationVO pTivoliAlertInfoVO)
			throws TivoliAlertException {

		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertBussinessBean handleEmailSenderEvent() method Start ::: ");
			}
			TivoliEmailSenderUtility temailsUtility = new TivoliEmailSenderUtility();
			TivoliAlertProperties prop = TivoliAlertProperties.getInstance();
			to = prop
					.getProperty(TivoliAlertConstants.MAIL_TO_RECIPIENT_ADDRESS);
			from = prop.getProperty(TivoliAlertConstants.MAIL_FROM_ADDRESS);
			cc = prop
					.getProperty(TivoliAlertConstants.MAIL_CC_RECIPIENT_ADDRESS);
			if(null!= pTivoliAlertInfoVO.getEmailGroupName() && pTivoliAlertInfoVO.getEmailGroupName().length() > 0)
			{
				String emailGroup=pTivoliAlertInfoVO.getEmailGroupName().trim();
				if (cIsDebug) {
					 cLogger.debug("Alert's emailGroup in TivoliAlertBussinessBean.handleEmailSenderEvent():: "+emailGroup);
				 }
				if(null != prop.getProperty(emailGroup+TivoliAlertConstants.EMAIL_TO) && prop.getProperty(emailGroup+TivoliAlertConstants.EMAIL_TO).length() > 0)
				{
					to = prop
							.getProperty(emailGroup+TivoliAlertConstants.EMAIL_TO);
				}
				if(null != prop.getProperty(emailGroup+TivoliAlertConstants.EMAIL_CC) && prop.getProperty(emailGroup+TivoliAlertConstants.EMAIL_CC).length() > 0)
				{
					cc = prop
							.getProperty(emailGroup+TivoliAlertConstants.EMAIL_CC);
				}
			}
			
			if (pTivoliAlertInfoVO.getCheckSituationRowCount()) {
				StringWriter subjectWriter =  new StringWriter();
				if (pTivoliAlertInfoVO.getSEVERITY().equalsIgnoreCase("ignore")) {
					actionreq = "Action Not Required ";
				} else {
					actionreq = "Action Required ";
				}
				subjectWriter.append("" + pTivoliAlertInfoVO.getAPPLICATION() + " | "
						+ pTivoliAlertInfoVO.getCATEGORY() + " | " + "" + pTivoliAlertInfoVO.getSEVERITY()
						+ " | " + "" + pTivoliAlertInfoVO.getRESOLVER_GROUP()
						+ " | " + "Tivoli Application Alert" + " | " + ""
						+ actionreq);
				subject = subjectWriter.toString();
			} else {
				subject = prop.getProperty(TivoliAlertConstants.TIVOLI_EMAIL_SUBJECT);
			}
			
			emailprinted = temailsUtility.sendEmail(to, cc, from, subject , pEmailTemplate);

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSSINESSBEAN,
					e.toString());
		}
		
		if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessBean handleEmailSenderEvent() method End ::: ");
		}
		return emailprinted;
	}

	/**
	 * This method handles activity of inserting alert details record in AlertStorage.xls file
	 */
	public boolean handleInsertEvent(TivoliAlertInformationVO taInformationVO) throws TivoliAlertException {
		if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessBean handleInsertEvent() method Start ::: ");
		}
		//Replace with Write excel file to store the alert.
		try {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
				Date now  = new Date();
				String lCurrentTime = formatter.format(now);
				
				TivoliAlertExcelWriter alertWriter =  new TivoliAlertExcelWriter();
				
				//In case of null VO write alertstr set in taInformationVO.nullalertvo property as it is and current time
				
				if(null!= taInformationVO && null!= taInformationVO.getAlertNullVO() && taInformationVO.getAlertNullVO().length() > 0)
				{
					alertWriter.writeToExcel(taInformationVO.getAlertNullVO(),"", "", "","", "",lCurrentTime);
					if (cIsDebug) {
						 cLogger.debug("Alert text in TivoliAlertBussinessBean.handleInsertEvent():: "+taInformationVO.getAlertNullVO());
					 }
				}
				else
				{
					alertWriter.writeToExcel(taInformationVO.getAlertMessage(), taInformationVO.getRESOLVER_GROUP(), 
							taInformationVO.getSEVERITY(), taInformationVO.getAPPLICATION(), taInformationVO.getSITUATION_ID_PK(),taInformationVO.getCONFIGURATION_ITEM(), lCurrentTime);
					if (cIsDebug) {
						 cLogger.debug("Alert description in TivoliAlertBussinessBean.handleInsertEvent():: "+taInformationVO.getSITUATION());
					 }
				}
				hievent=true;
				
		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSSINESSBEAN,
					e.toString());
		}
		
		if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessBean handleInsertEvent() method End ::: ");
		}
		return hievent;
	}
	
	/**
	 * This method handles  activity of new blank AlertStorage.xls file creation with headers only
	 * alert.
	 */
	public boolean handleStrorageFileCreateEvent() throws TivoliAlertException {
		try {
			validateResult = tivoliAlertExcelUtil.createAlertStorageXLS();
			if (cIsDebug) {
				cLogger.debug("Alert's validateResult in TivoliAlertBussinessBean.handleStrorageFileCreateEvent():::" +validateResult);
			}

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return validateResult;

	}
	
}